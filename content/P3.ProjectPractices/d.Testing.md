
### d.  Security testing

Security testing should verify that security requirements of all 4 sources (see 'D3-b' point above) are satisfied or if not then handled as open issues. 

New findings of testing should be registered as new issues according to 'D1-b', 'Security tickets handling'.

To maintain full coverage of testing the project should maintain the following practices:  

* Pre-release security assessment based on the verification standard.

* Security code review.

* Automated static testing.

* Peer code reviewing for security issues.

