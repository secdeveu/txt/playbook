
### g.   Release practices

See D2-e1, Quality gates, bug bars, and D2-e2, Release delivery conventions agreed on team level.

Also check for audit practices on Final Audit level. It should only confirm that everything was done well.

Review the documentation and assure that security related knowledge is present and proper.

Check if the incident response plan is up-to-date to deal with the production instance of the software.

Check the support SLA for security related stand-by and guaranties.




