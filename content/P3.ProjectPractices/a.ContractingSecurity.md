
### a.   Contracting security

Well-functioning of the secure development practices in a project greatly depends on the proper assignment and fulfillment of the security related roles in that development project. The agreement regarding roles and rules of game should cover project-internal roles, the involvement and standby of the project-external security specialists and also settle the handling of possible issues.

