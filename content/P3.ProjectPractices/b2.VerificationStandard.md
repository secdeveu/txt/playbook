
#### b2.     Verification standard

Verification standard creates common understanding of requirements and testing checks, and is used by architects, developers, testers and auditors alike.

Developers should be familiar with the chosen verification standard.
The project has to verify yearly that developers can assure their knowledge of the latest version of the standard.

**Versioning.** The project should be tested against the latest stable version of the chosen verification standard. As an example an ASVS checklist is the following:
[https://github.com/OWASP/ASVS/blob/master/OWASP%20Application%20Security%20Verification%20Standard%203.1.xlsx](https://github.com/OWASP/ASVS/blob/master/OWASP%20Application%20Security%20Verification%20Standard%203.1.xlsx)

The acceptable derogations from that standard are to be defined on project level explicitly. <!--XOX.-->

