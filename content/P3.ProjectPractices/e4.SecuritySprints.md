
#### e4.   Security sprints

Projects sometime collect a number of security issues which were not fixed due the tradeoffs of the lower severity of those issues vs other priorities, due to accepted workarounds, etc.

The project should devote at least one sprint or two weeks of the project runtime to solve and close security related tickets only. 

Make it part of the security sprint to review the derogations (accepted weakenings of security) and probably turn those into an issue to mitigate. (See D3-d5 regarding derogations).

