
### b.  Requirements

Keep the security requirements, design and guidelines explicit.  

The section *'C0-a'* defines the sources of the security quality requirements a project should comply with. 

Two of those sources 'a2' (Verification standard), 'a3' (Secdev guide) are security related requirements set out on the project level. See the relevant practices below.

The 'a4' source is 'Project security note' which type of adhoc requirements should be handled on sprint/continuous level, see relevant practices in section *'D4-c'*.

###### References
* SDL practices, Establish Security Requirements [2]  
* SDL practices, Establish Design Requirements [5]  
* SDL practices, Perform Security and Privacy Risk Assessments [4]  

