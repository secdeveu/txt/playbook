
### f.   Tooling

And again tooling comes to the picture on the project level as well. 

Besides of the tooling provided by the Factory and used by the team, in a project other tools may be employed in order to maintain compatibility with clients, subcontractors or services part of the project.

Same rules of caution apply. May a tool that we would not trust and install on our platforms is inevitable, find a solution for sandboxed installation.

