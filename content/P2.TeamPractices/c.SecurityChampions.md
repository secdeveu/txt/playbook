
### c.   Security champions

In order to prevent any misunderstanding: security champions are not security experts -- well, they might become those later -- but a key member of the development team, who is there to represent security aspects in the everyday practices of development, e.g. during development and evaluation meetings, at every moment of preparation and decision making.

Security champion is a normal member of a development team who assumes this long term responsible role of security champion. This role is voluntary and reflects the person's interest in security and willingness to undertake continuous actions to serve the secure development performance of the team. In return the person should get continuous benefits in terms of professional development and appreciation especially in relation to software security.

The major obligations of the security champion are:

* "Act as the 'voice' of security" at project/planning/scrum meetings;

* Help to make decisions about when to engage the security specialist;

* Be the security expert in the team, or the messenger between the security experts and the team;

* Maintain the 'CZ' part of the guide, the project security notes, see 'D4-c';

* Passively and actively monitor the radars where information regarding the 3rd party libraries vulnerabilities may arrive.

See also the referenced guides below.

###### References:
* [OWASP Security_Champions_Playbook](https://www.owasp.org/index.php/Security_Champions_Playbook)
* [https://www.owasp.org/index.php/Security_Champions](https://www.owasp.org/index.php/Security_Champions) 

