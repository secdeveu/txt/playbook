
### b.   Onboarding

Every new member of the team should:

* Learn the normative part of the secure development guide.

* Attend the a basic secure development/coding training as soon as possible, if never received such level of professional education.

* Attend the next advanced secure coding/development training available for other team members.

