
#### e1. Quality gates, bug bars

MS SDL:

"Defining minimum acceptable levels of security and privacy quality at the start helps a team understand risks associated with security issues, identify and fix security bugs during development, and apply the standards throughout the entire project."

"Setting a meaningful bug bar involves clearly defining the severity thresholds of security vulnerabilities (for example, no known vulnerabilities in the application with a 'critical' or 'important' rating at time of release) and never relaxing it once it's been set."

###### References  
* [SDL practices, Create Quality Gates/Bug Bars](https://www.microsoft.com/en-us/SDL/process/requirements.aspx) 

