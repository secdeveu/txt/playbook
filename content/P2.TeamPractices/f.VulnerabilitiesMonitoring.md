
### f. Vulnerabilities monitoring

A team responsible for support of closed development projects (aka live applications in production) should regularly monitor if security problems relevant to a supported application appear on available radars.

The primary source of such notifications is external feedback. Users should have a one-click-away option to submit their observations, or at least an email address with someone reading the messages daily. For corporate applications the channel to submit security related observations may be regulated in the support SLA.

The 3rd party libraries and modules are sources of danger. The trigger for patching our applications should work in a designed manner.

The team should appoint a member (the security champion) to actively check the issue trackers and official pages/blogs of the modules (developers).

For commercial modules the security notes may arrive according to the license or support agreements. The last mile between the arrival of that note to the company and the support team should work without possible failures.

For monitoring the vulnerabilities of open source modules there are services like Blackduck.

See also D3-c5, '3rd party modules inclusion' for the proactive strategy.

And finally if our security champion listens to security podcasts and is subscribed to newsletters or feeds of this nature she has great chances to be notified by the relevant alarms early.

