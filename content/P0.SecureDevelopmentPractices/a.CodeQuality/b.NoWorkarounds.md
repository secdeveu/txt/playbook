
#### b.   No workarounds

Only deploy security proxies (like a WAF) and other vulnerability shielding workarounds if in-the-code fix is not practicable or only for the temporary window between discovery of an issue and the deployment of a reliable fix.

Workarounds should be considered as weakening the software security by design. And as such the presence of a workaround should be registered as derogation in security documents. See also D3-d5, Application specifics regarding derogations.

Workarounds which are not registered as Application specific derogations (according to D3-d5) should be filed as security related issues with an open ticket, see D1-b, Security tickets handling.

