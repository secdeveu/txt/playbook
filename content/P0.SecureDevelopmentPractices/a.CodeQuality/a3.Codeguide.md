
##### a3.     Secure coding guide

The project specific standard secure development requirements should be defined in the normative part of the secure development guide assigned to the project or its subproject.

See section *'D3-a3'* for the project level definitions of the secdev guide applicable.

In case of the secdev\[eu\] coding guides this normative part are one or more applicable 'C' chapters.

