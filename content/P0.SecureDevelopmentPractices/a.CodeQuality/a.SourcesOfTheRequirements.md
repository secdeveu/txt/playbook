
#### a.  Sources of the security quality requirements

The rules regarding the security compliance of any code are the following:

1. The factory-wide common requirements are set out in the current 'C0' part of the playbook. 

2. All projects/applications have to comply with a security verification standard accepted by the industry as best practice (eg. OWASP ASVS). 

3. Secure development guide. 

4. Project security notes. Ad-hoc security rules defined on the level of the project practices.

5. Applications may implement special security related features or may have specific hardening requirements.

