
#### e3. Monitoring

#####    Security related feedback

Vulnerabilities of applications has much common with hidden defects which can not be detected during in-production and acceptance testing. Many security problems will surface during the real-time usage. Applications being used by the clients and in the internal departments will generate the issues which are to be captured for further analysis and treatment.

Make it dead simple to submit security related issues or suspicions. Error collection feature assisted by the user is the modern solution, though clearly providing a feedback email address may not be that fancy a feature but does the critical part of the job.

For public applications periodically offering a bug bounty program may also be a coordinated crowd-based process of capturing security problems.
[https://en.wikipedia.org/wiki/Bug_bounty_program](https://en.wikipedia.org/wiki/Bug_bounty_program)

#####    Vulnerabilities monitoring

Roughly speaking the cheapest way to find vulnerabilities and other security problems in platforms, tools and services used by the organization is to read or hear about them, and the react quickly.

Section D2-f explains vulnerabilities monitoring in 3rd party modules of a software developed or supported by a project. Similar approach applies to other software used by the organization.

#####    SIEM, audit logs

The advanced form of monitoring is SIEM, when our automated systems can detect incidents and deliver security alerts by collecting events data on many sensors and analyse that in real-time.

Audit logs are the minimal security feature of this kind that should be implemented on all systems of significant business risk.

Dealing with audit logs is also a practice which should be implemented on Factory level.

