
### c.   Security experts

##### Security management

The proper implementation of the S-SDLC (the playbook) in the Factory and the projects is responsibility of the security management.

There are also many tasks in implementing the organizational context for S-SDLC that is not part of the playbook. For example cooperation of the business, the product owners, project managers, the development teams and leaders, and the security teams is to be tuned.

The classic problem of security that it does not bring tangible value so by default motivations to implement new features suppress the motivation to devote significant time and efforts on S-SDLC practices.

##### Security engineers

The security team of Factory is the first line support for the developers.

##### Security consultants

The specialized security consulting and auditing competence are often provided by external application security professionals and testing experts.

The cost and efficacy of employing these experts can be optimized by:

* Scheduling their services;  
* Timely providing clear, complete and valid information for them to prepare realistic estimations;  
* Having a clever stand-by agreement to ask for this second line of support in case of difficulties with internal handling of security related problems.

