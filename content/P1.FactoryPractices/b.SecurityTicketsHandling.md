
### b. Security tickets handling

Tracking of issues with information related to potential vulnerabilities or other security problems that could help malicious actions should be regulated on factory level ensuring efficient and proper cross-team, cross-department, cross-domain handling of such issues/tickets. The established practice should result in:

* Protection of the confidential information.

* Straight flow of handling of such issues from the moment of suspicion to the change of the status of the issue to it bearing no practical risk anymore.

* Following the mitigation/solution of the risks: internal disclosure of those details of the problem that could help organizational learning.

Details of the security related issues that affect any vulnerable system should not be included in the issue tracking accessible by developers or other persons not involved in fixing or mitigating of the given security problem. 

If the issue does not affect systems/applications in production or in rollout phase, and the fix (not a workaround) of the issue long before the rollout is the natural expectation, then the above clause does not apply. Thus security issues in the early stage of the development project should be tracked as normal.

The issue tracking system used in the development process should include relevant ticket or tickets assigned to those involved in the fixing/mitigation process. Instead of storing the relevant information in the ticket or other non security related development storage, the ticket should only convey a reference to the corresponding document/folder in the security related storage. The title and the texts in the ticket should not convey any hint regarding the risks or exploitation of the vulnerability/issue. <!--The ticket should contain the XOX.-->

The security related issue storage should be encrypted and enforce adequate access control. The access control should limit the access to the details of any given issue to those involved in fixing or mitigation of the given security problem. 

The proper storage for the security related issues is defined as 'confidential document store' in D2-d1.

When the last affected system seized to be vulnerable and the issue is closed the responsible security engineer should copy over all the relevant details into the ticket and notify the security champion of the involved project that the details of the security issue are available for discussion.

Within two weeks following the disclosure the team of the project should decide if the issue represents a story to learn from. If so the issue is to be added to the project security notes (see section *'D4-c'*) and a postmortem discussion is to be scheduled. 



