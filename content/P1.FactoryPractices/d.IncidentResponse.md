
### d.   Incident response

Incident response capabilities a naturally implemented on Factory level.

The incident response how-to is out-of-scope of the secure development.

The incidents management should by design be properly channeled into:

* The software (project) support process and issue tracking;   
* The learning process of the development teams affected by the exploits (see also postmortem learning).  

It is strongly recommended to avoid bashing developers in case of incidents with root causes in vulnerabilities or other software defects. Because the root cause can be malfunction of the security QA which is collective responsibility of the Factory.



