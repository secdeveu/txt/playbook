
# D1.  Factory practices

On the Factory level the practices should facilitate the circumstances in which functioning of the team and project level secure development focuses only on the fulfillment of the norms delegated to their level.

