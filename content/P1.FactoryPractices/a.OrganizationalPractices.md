
### a.  Organizational treatment of applications security

Integration of secure software development into corporate development culture enables that security is paid due attention throughout the development lifecycle, in every phase from planning to maintaining.

From the corporate governance perspective the topic of application security should be declared as high priority on the top level of the organization. It should be designed so and verified yearly that the implications of this priority are the following:

* Communication regarding application security risks, issues and actions is encouraged. 

* All issues with potential to result in security breaches are documented and known to managers responsible for security quality.

* Development teams attend secure coding/development trainings yearly.

* Application development projects devote 5% of the effective design, production and testing efforts to ensure that the software products feature good security quality.

* The development projects regularly communicate with security engineers regarding issues beyond their competence.

* High and critical issues discovered during audits are properly discussed by the affected teams and persons. 

* The secure development guides and policies are up-to-date with the development realities and the actual conduct.

* The root causes of the security problems are known and finally handled.

* The exceptions/deviations of the real practice from the secure development regulations are known and the associated risks are explicitly taken by responsible managers.

##### Regulatory context

'ISO/IEC DIS 27034-3', 'Information technology -- Application security -- Part 3: Application security management process' standard is expected to be approved and published Q1 2018, see:
[https://www.iso.org/standard/55583.html](https://www.iso.org/standard/55583.html) .

The publication of the above standard may affect the regulations of the playbook.

