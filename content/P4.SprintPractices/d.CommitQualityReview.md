
### d. Code quality review prior to committing 

Developers are advised to spend an hour every two days reviewing the quality of their new codes. Not the security quality specifically but the code quality in many aspects of it. Security related concerns are expected to also be reviewed during this process without special dedication.

Such a review can be scheduled as preparation for committing changes.

The commit quality review can be performed as peer review as well, see D3.d2.