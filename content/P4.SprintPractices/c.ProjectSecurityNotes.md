
### c.    Project security workbook and notes

Situations may emerge that bring new security related requirements, prescriptions or issues. These new normative definitions may be set out in for example:  

* vulnerability assessments / penetration test results, VAPT reports;  
* new specification documents that convey security requirements;  
* user stories and task definitions;  
* sprint notes;   
* a ticket,  
* and the team may come up with an improvement to security.  

It is the security champion's responsibility to capture these emerging requirements and immediately register those as 'security notes' in the 'Project security workbook'. 

> See the template in the chapter 'CZ' of the coding guide. See chapter 'W', the editor's how-to for instructions to add new sections to the guide. 
> The 'Project security workbook' can be implemented in any content/document management solution with which the project members are comfortable in terms of handy recording and accessing the notes. It is advisable to use solutions with addressable parts (link sharing). 

The project security workbook should be protected by means of access management.

The rules of D1.b, 'Security tickets handling' are applicable to the sensitive content parts of the security notes.

The project/planning meetings should review the new security notes analyze what risk management implications the new security requirement highlights or the issue brings and if changes are to be applied to the threat model and attack surface model.

