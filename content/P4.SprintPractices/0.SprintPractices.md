
# D4. Sprint/continuous practices

In the view of this playbook "sprints" are a week or two or maximum a month long terms of a project with well-defined outcomes. This view fits not only agile projects and scrum methodologies. We assume that sprint-like time-boxed shorter iterations are building blocks of every project when participants focus on achieving short-term results.
The secure development sprint practices exists in an already established environment and are concerned with applying the ready/at-hand methods and tooling of secure development in the ongoing works.

<!--
Treat and surface monitoring of changes
Security tickets

Every-sprint practices of MS SDL
Development procedures (includes ongoing tests)
Chain security
Security related tickets handling
-->


